var mysql = require("mysql");
var pool;


function doConnect(user, password){
    var config = {
      connectionLimit : 1000,
      host     : '98.142.99.218',
      //host : 'localhost',
      debug    :  false,
      acquireTimeout : 20000
    };
    config.user = user;
    config.password = password;
    config.database = user;
    pool = mysql.createPool(config);
    pool.getConnection(function(error, connection){
      if(error){
        alert("Username or Password Is Wrong");
        console.log(error);
      }
      else{
        $('#branch-name').html('Branch: ' + user);
        $('#login-phase').hide();
        $('#work-phase').removeClass('hide');
        var date = new Date().toJSON().slice(0,10);
        $('input[type="date"]').val(date);
        connection.release();
        initbankaccount();
        syncloop();
      }
    });
}