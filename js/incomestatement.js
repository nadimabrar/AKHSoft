function  getincomestatement(Date1, Date2){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("call getValue(?)",[Date1],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        var Value = result[0][0].Value;
        connection.query("call getIncomeStatement(?, ?)",[Date1, Date2],function(error, result, fields){
          if(error){
            alert("Something is wrong");
            console.log(error);
          }
          else{
            var top = {};
            top.revenue = {};
            top.expense = {};
            var graphdata = {};
            var income = 0;
            var dates = [];
            for(var d = new Date(Date1); d<=new Date(Date2); d.setDate(d.getDate() + 1)){
              dates.push(new Date(d).toJSON().slice(0,10));
              graphdata[new Date(d).toJSON().slice(0,10)]=0;
            }
            for(i in result[0]){
              graphdata[new Date(result[0][i].Date).toJSON().slice(0,10)]+=result[0][i].Credit-result[0][i].Debit;
              if(result[0][i].Debit == 0){
                if(!(result[0][i].Class in top.revenue)) top.revenue[result[0][i].Class]=0;
                top.revenue[result[0][i].Class]+=result[0][i].Credit;
                income += result[0][i].Credit;
              }
              else{
                if(!(result[0][i].Class in top.expense)) top.expense[result[0][i].Class]=0;
                top.expense[result[0][i].Class]+=result[0][i].Debit;
                income -= result[0][i].Debit;
              }
            }
            console.log(JSON.stringify(graphdata));
            for(var d = new Date(Date1); d<=new Date(Date2); d.setDate(d.getDate() + 1)){
              console.log(new Date(d));
            }
            var display = '<div class="col-md-6 col-md-offset-3">';
            display += 'Opening Value: <div style="float:right">'+Value+'</div><br>';
            display += '<b> Revenues(+) </b><br>';
            for(row in top.revenue){
              display += row+': <div style="float:right">'+top.revenue[row]+'</div><br>';
            }
            display += '<b> Expenses(-) </b><br>';
            for(row in top.expense){
              display += row+': <div style="float:right">'+top.expense[row]+'</div><br>';
            }
            display += '<div style="border-top: 1px solid gainsboro"></div>';
            display += 'Net Income: <div style="float:right">'+income+'</div><br>';
            income += Value;
            display += 'Closing Value: <div style="float:right">'+income+'</div><br>';
            
            display += '</div><br>';
            display += ObjtoTable(result[0]); 
            $('#Statement').html(display);
            var step=dates.length;
            step= Math.floor(step/6);
            if(step == 0) step=1;
            for(i=0;i<dates.length-1;i++){
              if(i%step != 0){
                dates[i]="";
              }
            }
            var datas = [];
            for(key in graphdata){
              Value += graphdata[key];
              datas.push(Value);
            }
            console.log(dates);
            console.log(datas);
            var data = {
              labels: dates
              ,
              datasets: [
                      {
                          label: "Product Analysis",
                          fillColor: "rgba(151,187,205,0.2)",
                          strokeColor: "rgba(151,187,205,1)",
                          pointColor: "rgba(151,187,205,1)",
                          pointStrokeColor: "#fff",
                          pointHighlightFill: "#fff",
                          pointHighlightStroke: "rgba(151,187,205,1)",
                          data: datas
                      }
                  ]
            };
            var ctx = document.getElementById("StatementGraph").getContext("2d");
            var options = {};
            var Graph = new Chart(ctx).Line(data, options);
          }
        });
      }
    });
  });
}