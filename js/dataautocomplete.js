var fs = require('fs');

var PartyData, SupplierData, GoodDataBrand, GoodDataProduct, DataClass;

function initAutoComplete(){
  var optionPartyName = {
    minLength : 1,
    source : PartyData,
    select : function (event, ui){
      latest.val(ui.item.value);
      latest.parents().eq(3).find('input[name="Party_Id"]').first().val(ui.item.Party_Id);
      latest.parents().eq(3).find('input[name="Party_Address"]').first().val(ui.item.Party_Address);
      latest.parents().eq(3).find('input[name="Mobile_No"]').first().val(ui.item.Mobile_No);
      latest.parents().eq(3).find('input[name="Billing_Address"]').first().val(ui.item.Party_Address);
      return false;
    }
  };

  var optionSupplierName = {
    minLength : 1,
    source : SupplierData,
    select : function (event, ui){
      latest.val(ui.item.value);
      latest.parents().eq(3).find('input[name="Supplier_Id"]').first().val(ui.item.Supplier_Id);
      latest.parents().eq(3).find('input[name="Supplier_Address"]').first().val(ui.item.Supplier_Address);
      latest.parents().eq(3).find('input[name="Mobile_No"]').first().val(ui.item.Mobile_No);
      return false;
    }
  };

  var optionGoodBrand = {
    minLength : 1,
    source : GoodDataBrand,
    select : function (event, ui){
      latest.parents().eq(1).find('input[name="Good_Id"]').first().val(ui.item.Good_Id);
      latest.val(ui.item.value);
      latest.parents().eq(1).find('input[name="Product"]').first().val(ui.item.Product);
      latest.parents().eq(1).find('input[name="Unit"]').first().val(ui.item.Unit);
      return false;
    }
  };

  var optionGoodProduct = {
    minLength : 1,
    source : GoodDataProduct,
    select : function (event, ui){
      latest.parents().eq(1).find('input[name="Good_Id"]').first().val(ui.item.Good_Id);
      latest.parents().eq(1).find('input[name="Brand"]').first().val(ui.item.Brand);
      latest.val(ui.item.value);
      latest.parents().eq(1).find('input[name="Unit"]').first().val(ui.item.Unit);
      return false;
    }
  };
  
  var optionClass = {
    minLength : 1,
    source : DataClass
  };

  $('input[name="Party_Name"]').each(function(){
    $(this).autocomplete(optionPartyName).autocomplete( "instance" )._renderItem = function( ul, item ) {
     return $( "<li>" )
     .append( "<div><p><b>" + item.value + "</b><br>" + item.Party_Address + "<br>" + item.Mobile_No + "</p></div>" )
     .appendTo( ul );
    };
  });
  $('input[name="Supplier_Name"]').each(function(){
    $(this).autocomplete(optionSupplierName).autocomplete( "instance" )._renderItem = function( ul, item ) {
     return $( "<li>" )
     .append( "<div><p><b>" + item.value + "</b><br>" + item.Supplier_Address + "<br>" + item.Mobile_No + "</p></div>" )
     .appendTo( ul );
    };
  });
  $('input[name="Brand"]').each(function(){
    $(this).autocomplete(optionGoodBrand).autocomplete( "instance" )._renderItem = function( ul, item ) {
     return $( "<li>" )
     .append( "<div><p>" + item.value + "<br>" + item.Product + " (" + item.Unit + ")</p></div>" )
     .appendTo( ul );
    };
  });
  $('input[name="Product"]').each(function(){
    $(this).autocomplete(optionGoodProduct).autocomplete( "instance" )._renderItem = function( ul, item ) {
     return $( "<li>" )
     .append( "<div><p>" + item.Brand + "<br>" + item.value + "(" + item.Unit + ")</p></div>" )
     .appendTo( ul );
    };
  });
  $('input[name="Class"]').each(function(){
    $(this).autocomplete(optionClass).autocomplete( "instance" )._renderItem = function( ul, item ) {
     return $( "<li>" )
     .append( "<div>" + item.value + "</div>" )
     .appendTo( ul );
    };
  });
}


function syncautocompletedata(){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Something is wrong with the connection!");
    }
    else{
      connection.query("SELECT `Party_Id`, (`Party_Name`) `value` , `Party_Address`, `Mobile_No` FROM `Party`",function(error, result, fields){
        if(error){
          alert('Something is wrong!');
        }
        else{
          PartyData = result;
          connection.query("SELECT  (`Supplier_Name`) `value` , `Supplier_Id`, `Supplier_Address`, `Mobile_No` FROM `Supplier`",function(error, result, fields){
            if(error){
              alert('Something is wrong!');
            }
            else{
              SupplierData = result;
              connection.query("SELECT (`Brand`) `value` , `Good_Id`, `Product`, `Unit` FROM `Good`",function(error, result, fields){
                if(error){
                  alert('Something is wrong!');
                }
                else{
                  GoodDataBrand = result;
                  connection.query("SELECT (`Product`) `value` , `Good_Id`, `Brand` , `Unit` FROM `Good`",function(error, result, fields){
                    if(error){
                      alert('Something is wrong!');
                    }
                    else{
                      GoodDataProduct = result;
                      connection.query("SELECT DISTINCT (`Class`) `value` FROM `Utility_Info`",function(error, result, fields){
                        if(error){
                          alert('Something is wrong!');
                        }
                        else{
                          DataClass = result;
                          initAutoComplete();
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}