 function getmemostatus(connection, listMemo, table){
   var Memo_Id = listMemo.pop().Memo_Id;
   connection.query("SELECT `Date`, `Memo_Id`, `DO_No`, `Billing_Address`,`Sub_Total`, `Load_Amount`, `Transport`, `Total`, `Paid`, `Discount`, `Due` FROM `Memo_Info` WHERE Memo_Id = ?",[Memo_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
        var Memo = result[0];
        console.log(JSON.stringify(Memo));
         connection.query("SELECT `Brand`, `Product`, `Unit`, `Quantity`, `Rate`, `Amount` FROM `Good` LEFT JOIN `Memo_Purchase` ON Good.Good_ID = Memo_Purchase.Good_ID WHERE Memo_Id = ?",[Memo_Id],function(error, purchase, fields){
            if(error){
              alert("Something is wrong");
            }
            else{
              var design = '';
              Memo.purchases = purchase;
              design += '<div class="col-md-12 list-box">';
              design += '<div class="col-md-9">';
              design += '<div><b>Memo Id #'+Memo.Memo_Id+'</b> (Date: '+Memo.Date.toString().slice(0,15)+')</div>';
              design += ObjtoTable(Memo.purchases);
              design += '<div>Billing Address: '+Memo.Billing_Address+'</div>';
              design += '</div>';
              design += '<div class="col-md-3" style="position: absolute; bottom: 15px; right: 15px">';
              design += 'Sub Total: <div style="float:right">'+Memo.Sub_Total+'</div><br>';
              design += 'Load Amount: <div style="float:right">'+Memo.Load_Amount+'</div><br>';
              design += 'Transport: <div style="float:right">'+Memo.Transport+'</div><br>';
              design += '<div style="border-top: 1px solid grey"> </div>';
              design += 'Total: <div style="float:right">'+Memo.Total+'</div><br>';
              design += 'Paid: <div style="float:right">'+Memo.Paid+'</div><br>';
              design += 'Discount: <div style="float:right">'+Memo.Discount+'</div><br>';
              design += '<div style="border-top: 1px solid grey"></div>';
              design += 'Due: <div style="float:right">'+Memo.Due+'</div><br>';
              design +=  '</div>';
              design +=  '</div>';
              table.append(design);
              getmemostatuslist(connection, listMemo, table);
            } 
        });
      }
    });
 }

function getmemostatuslist(connection, listMemo, table){
  console.log(JSON.stringify(listMemo));
    if(listMemo.length == 0){
      return;
    }
    else{
      getmemostatus(connection, listMemo,table);
    }
}



function getpartystatement(partyObj, table){
  console.log(JSON.stringify(partyObj));
  var design = '';
 design += '<div class="col-md-6"><table class="table table-no-border table-condensed"><tbody>';
 design += '<tr><td class="col-md-3">Party Name:</td><td class="col-md-9">'+partyObj.Party_Name+'</td></tr>';
 design += '<tr><td class="col-md-3">Party Address:</td><td class="col-md-9">'+partyObj.Party_Address+'</td></tr>';
 design += '<tr><td class="col-md-3">Mobile No:</td><td class="col-md-9">'+partyObj.Mobile_No+'</td></tr>';
 design += '</tbody></table></div>';
 design += '<div class="col-md-3 col-md-offset-2">';
 design += 'Total Due:<div style="float:right">'+0+'</div><br>';
 design +=  '</div>';
 design +=  '<div class="col-md-12" style="margin-bottom: 10px; border-top: 1px solid grey"></div>';
 design += '<div class="col-md-12"><ul class="nav nav-pills nav-justified">';
 design += '<li class="active"><a data-toggle="pill" href="#Memo_Tab">Purchases</a></li>';
 design += '<li><a data-toggle="pill" href="#Forward_Tab">Forward Purchases</a></li>';
 design += '<li><a data-toggle="pill" href="#Due_Tab">Due Payment</a></li>';
 design += '</ul></div>';
 design += '<div class="tab-content"><div class="tab-pane fade in active" id="Memo_Tab"></div>';
 design += '<div class="tab-pane fade" id="Forward_Tab"></div>';
 design += '<div class="tab-pane fade" id="Due_Tab"></div></div>';
 table.html(design);
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("SELECT `Memo_Id` FROM `Memo_Info` WHERE Party_Id = ? ORDER BY `Date`",[partyObj.Party_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
         getmemostatuslist(connection, result, $('#Memo_Tab'));
         connection.query("SELECT `Date`, `Memo_Id`, `DO_No`, `Total`, `Paid`, `Discount`, `Due` FROM `Forward_Memo_Info` WHERE Party_Id = ? ORDER BY `Date`",[partyObj.Party_Id],function(error, result, fields){
           if(error){
              alert("Something is wrong");
            }
            else{
              var design = '';
              generateforwardrows(connection, result, $('#Forward_Tab'), design);
              connection.query("SELECT `Date`, (`Value`)`Amount` FROM `Due_Payment` WHERE Party_Id=? ORDER BY Date DESC",[partyObj.Party_Id],function(error, result, fields){
               if(error){
                  alert("Something is wrong");
                }
                else{
                 $('#Due_Tab').html('<div class="col-md-6 col-md-offset-3">'+ObjtoTable(result)+'</div>');
                }
              });
            }
          });
      }
    });
  });
}