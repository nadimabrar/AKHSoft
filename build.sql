DROP TABLE `Limit`;
DROP TABLE `Interest`;
DROP TABLE `Transaction`;
DROP TABLE `Bank_Accounts`;
DROP TABLE `Forward_Good_Delivery_Good`;
DROP TABLE `Forward_Good_Delivery`;
DROP TABLE `Forward_Good_Purchase`;
DROP TABLE `Forward_Good_Info`;
DROP TABLE `Forward_Memo_Delivery_Good`;
DROP TABLE `Forward_Memo_Delivery`;
DROP TABLE `Forward_Memo_Purchase`;
DROP TABLE `Forward_Memo_Info`;
DROP TABLE `Utility_Info`;
DROP TABLE `Good_Purchase`;
DROP TABLE `Good_Info`;
DROP TABLE `Memo_Purchase`;
DROP TABLE `Memo_Info`;
DROP TABLE `Stock`;
DROP TABLE `Supplier`;
DROP TABLE `Party`;
DROP TABLE `Good`;


CREATE TABLE `Good` ( 
  `Good_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Brand` TEXT NOT NULL,
  `Product` TEXT NOT NULL,
  `Unit` TEXT NOT NULL,
  PRIMARY KEY (`Good_Id`)
);


CREATE TABLE `Party` ( 
  `Party_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Party_Name` TEXT NOT NULL,
  `Party_Address` TEXT NOT NULL,
  `Mobile_No` TEXT NOT NULL,
  PRIMARY KEY (`Party_Id`)
);


CREATE TABLE `Supplier` ( 
  `Supplier_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Supplier_Name` TEXT NOT NULL,
  `Supplier_Address` TEXT NOT NULL, 
  `Mobile_No` TEXT NOT NULL,
  PRIMARY KEY (`Supplier_Id`)
);


CREATE TABLE `Stock` (
  `Good_Id` INT(20) NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);

CREATE TABLE `Memo_Info` ( 
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL, 
  `DO_No` TEXT NOT NULL, 
  `Party_Id` INT(20) NOT NULL,
  `Billing_Address` TEXT NOT NULL,
  `Sub_Total` DOUBLE NOT NULL,
  `Load_Amount` DOUBLE NOT NULL,
  `Transport` DOUBLE NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Memo_Id`),
  FOREIGN KEY (`Party_Id`) REFERENCES Party(`Party_Id`)
);



CREATE TABLE `Memo_Purchase` ( 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL,
  `Rate` DOUBLE NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  `Amount` DOUBLE NOT NULL,
  FOREIGN KEY (`Memo_Id`) REFERENCES Memo_Info (`Memo_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);


CREATE TABLE `Good_Info` ( 
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL, 
  `DO_No` TEXT NOT NULL, 
  `Supplier_Id` INT(20) NOT NULL,
  `Sub_Total` DOUBLE NOT NULL,
  `Load_Amount` DOUBLE NOT NULL,
  `Transport` DOUBLE NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Memo_Id`),
  FOREIGN KEY (`Supplier_Id`) REFERENCES Supplier (`Supplier_Id`)
);



CREATE TABLE `Good_Purchase` ( 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL,
  `Rate` DOUBLE NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  `Amount` DOUBLE NOT NULL,
  FOREIGN KEY (`Memo_Id`) REFERENCES Good_Info (`Memo_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);


CREATE TABLE `Utility_Info` ( 
  `Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL, 
  `Class` TEXT NOT NULL,
  `Note` TEXT NOT NULL, 
  `Type` TEXT NOT NULL,
  `Amount` DOUBLE NOT NULL,
  PRIMARY KEY (`Id`)
);


CREATE TABLE `Forward_Memo_Info` ( 
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL, 
  `DO_No` TEXT NOT NULL, 
  `Party_Id` INT(20) NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Memo_Id`),
  FOREIGN KEY (`Party_Id`) REFERENCES Party (`Party_Id`)
);


CREATE TABLE `Forward_Memo_Purchase` ( 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL, 
  `Rate` DOUBLE NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  `Amount` DOUBLE NOT NULL,
  FOREIGN KEY (`Memo_Id`) REFERENCES Forward_Memo_Info (`Memo_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);


CREATE TABLE `Forward_Memo_Delivery` (
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `Delivery_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `DO_No` TEXT NOT NULL,
  `Delivery_Address` TEXT NOT NULL,
  `Load_Amount` DOUBLE NOT NULL,
  `Transport` DOUBLE NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Delivery_Id`),
  FOREIGN KEY (`Memo_Id`) REFERENCES Forward_Memo_Info (`Memo_Id`)
);


CREATE TABLE `Forward_Memo_Delivery_Good` ( 
  `Delivery_Id` INT(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  FOREIGN KEY (`Delivery_Id`) REFERENCES Forward_Memo_Delivery (`Delivery_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);

CREATE TABLE `Forward_Good_Info` ( 
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL, 
  `DO_No` TEXT NOT NULL, 
  `Supplier_Id` INT(20) NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Memo_Id`),
  FOREIGN KEY (`Supplier_Id`) REFERENCES  Supplier(`Supplier_Id`)
);



CREATE TABLE `Forward_Good_Purchase` ( 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL, 
  `Rate` DOUBLE NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  `Amount` DOUBLE NOT NULL,
  FOREIGN KEY (`Memo_Id`) REFERENCES Forward_Good_Info (`Memo_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);


CREATE TABLE `Forward_Good_Delivery` ( 
  `Date` DATE NOT NULL, 
  `Memo_Id` VARCHAR(20) NOT NULL,
  `DO_No` TEXT NOT NULL,
  `Delivery_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Delivery_Address` TEXT NOT NULL,
  `Load_Amount` DOUBLE NOT NULL,
  `Transport` DOUBLE NOT NULL,
  `Total` DOUBLE NOT NULL,
  `Paid` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  `Due` DOUBLE NOT NULL,
  PRIMARY KEY (`Delivery_Id`),
  FOREIGN KEY (`Memo_Id`) REFERENCES Forward_Good_Info (`Memo_Id`)
);


CREATE TABLE `Forward_Good_Delivery_Good` ( 
  `Delivery_Id` INT(20) NOT NULL,
  `Good_Id` INT(20) NOT NULL,
  `Quantity` DOUBLE NOT NULL,
  FOREIGN KEY (`Delivery_Id`) REFERENCES Forward_Good_Delivery (`Delivery_Id`),
  FOREIGN KEY (`Good_Id`) REFERENCES Good (`Good_Id`)
);


CREATE TABLE `Bank_Accounts` (
  `Bank_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Bank_Name` TEXT NOT NULL,
  `Account_No` TEXT NOT NULL,
  PRIMARY KEY (`Bank_Id`)
);


CREATE TABLE `Transaction` (
  `Transaction_Id` INT(20) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL, 
  `Bank_Id` INT(20) NOT NULL,
  `Type` TEXT NOT NULL,
  `Note` TEXT NOT NULL,
  `Amount` DOUBLE NOT NULL,
   PRIMARY KEY (`Transaction_Id`),
   FOREIGN KEY (`Bank_Id`) REFERENCES Bank_Accounts (`Bank_Id`)
);

CREATE TABLE `Interest` (
  `Interest_Id` INT (20) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL, 
  `Bank_Id` INT (20) NOT NULL,
  `Value` DOUBLE NOT NULL,
  UNIQUE (`Date`,`Bank_Id`),
   PRIMARY KEY (`Interest_Id`),
   FOREIGN KEY (`Bank_Id`) REFERENCES Bank_Accounts (`Bank_Id`)
);




CREATE TABLE `Limit` (
  `Interest_Id` INT (20) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL, 
  `Bank_Id` INT (20) NOT NULL,
  `Value` DOUBLE NOT NULL,
  PRIMARY KEY (`Interest_Id`),
   FOREIGN KEY (`Bank_Id`) REFERENCES Bank_Accounts (`Bank_Id`)
);


CREATE TABLE `Due_Payment` (
  `Id` INT (20) NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL, 
  `Party_Id` INT (20) NOT NULL,
  `Value` DOUBLE NOT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`Party_Id`) REFERENCES `Party` (`Party_Id`)
);

DROP PROCEDURE IF EXISTS filldates;
DELIMITER |
CREATE PROCEDURE filldates(dateStart DATE, dateEnd DATE)
BEGIN
  CREATE TABLE `Dates` (`Date` DATE NOT NULL); 
  WHILE dateStart <= dateEnd DO
    INSERT INTO `Dates` (`Date`) VALUES (dateStart);
    SET dateStart = date_add(dateStart, INTERVAL 1 DAY);
  END WHILE;
END;
|
DELIMITER ;


CALL filldates('2010-01-01','2030-12-31');


DROP PROCEDURE IF EXISTS getBalanceAndInterest;
DELIMITER |
CREATE PROCEDURE getBalanceAndInterest(iniDate DATE, Bid INT(20))
BEGIN
  SET @Now := 0;
  SET @Cum := 0;
  SET @Int := (SELECT SUM(ROUND((IF(ISNULL(B.Value),@Now,@Now:=B.Value)*(@Cum := @Cum + IF(ISNULL(C.Balance),0,C.Balance)))/36000,2)) Interest FROM (SELECT `Date` FROM `Dates` WHERE `Date` <= iniDate) A LEFT JOIN (SELECT Date, Value FROM `Interest` WHERE `Date` <= iniDate AND Bank_Id = Bid) B ON A.Date = B.Date LEFT JOIN (SELECT Date, SUM(CASE Type WHEN 'Credit' THEN Amount ELSE -Amount END) Balance FROM `Transaction` WHERE `Date` <= iniDate AND Bank_Id = Bid GROUP BY Date) C ON A.Date = C.Date);
  SET @Int := IF(ISNULL(@Int),0,@Int);
  SET @IntP := (SELECT SUM(Amount) FROM `Transaction` WHERE Type='Interest' AND `Date` <= iniDate AND Bank_Id = Bid); 
  SET @IntP := IF(ISNULL(@IntP ),0,@IntP);
  SELECT (ROUND(@Cum,2)) `Balance` , (ROUND(@Int-@IntP,2)) `Interest`;
END;
|
DELIMITER ;


DROP PROCEDURE IF EXISTS getBalanceSheet;
DELIMITER |
CREATE PROCEDURE getBalanceSheet(Date1 DATE, Date2 DATE, Bid INT(20))
BEGIN
  SELECT * FROM (SELECT A.Date, C.Balance, (B.Value) Interest, C.Interest_Pay FROM
  (SELECT `Date` FROM `Dates` WHERE `Date` <= Date2) A LEFT JOIN (SELECT Date, Value FROM `Interest` WHERE `Date` <= Date2 AND Bank_Id = Bid) B ON A.Date = B.Date LEFT JOIN (SELECT Date, SUM(CASE Type WHEN 'Credit' THEN Amount ELSE -Amount END) Balance, SUM(CASE Type WHEN 'Interest' THEN Amount ELSE 0 END)  Interest_Pay FROM `Transaction` WHERE `Date` <= Date2 AND Bank_Id = Bid GROUP BY Date) C ON A.Date = C.Date) D WHERE D.Date >= Date1 ORDER BY D.Date;
END;
|
DELIMITER ;


DROP PROCEDURE IF EXISTS getProductAnalysis;
DELIMITER |
CREATE PROCEDURE getProductAnalysis(Date1 DATE, Date2 DATE, Gid INT(20))
BEGIN
  SELECT F.Date Date, IF(ISNULL(G.Quantity),0,G.Quantity) Quantity FROM (SELECT Date FROM Dates WHERE Date BETWEEN Date1 AND Date2) F LEFT JOIN (SELECT E.Date, SUM(E.Quantity) Quantity FROM (SELECT B.Date, A.Quantity FROM Memo_Purchase A LEFT JOIN Memo_Info B ON A.Memo_Id=B.Memo_Id WHERE B.Date BETWEEN Date1 AND Date2 AND Good_Id = Gid UNION SELECT D.Date, C.Quantity FROM Forward_Memo_Purchase C LEFT JOIN Forward_Memo_Info D ON C.Memo_Id = D.Memo_Id WHERE D.Date BETWEEN Date1 AND Date2 AND Good_Id = Gid) E GROUP BY E.Date) G ON F.Date = G.Date ORDER BY Date;  
END;
|
DELIMITER ;


DROP PROCEDURE IF EXISTS getIncomeStatement;
DELIMITER |
CREATE PROCEDURE getIncomeStatement(Date1 DATE, Date2 DATE)
BEGIN 
  SELECT * FROM
  (SELECT Date, (Memo_Id) Id, ('Sell Revenue') Class, (Sub_Total-Discount) Credit, 0 Debit FROM Memo_Info WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, (Memo_Id) Id, ('Delivery Revenue') Class, (Load_Amount+Transport) Credit, 0 Debit FROM Memo_Info WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, (Memo_Id) Id, ('Forward Sell Revenue') Class, (Total-Discount) Credit, 0 Debit FROM Forward_Memo_Info WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, (Memo_Id) Id, ('Delivery Revenue') Class, (Total-Discount) Credit, 0 Debit FROM Forward_Memo_Delivery WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, (Memo_Id) Id, ('Puschase Expense') Class, 0 Credit, (Total-Discount) Debit FROM Good_Info WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, Id, Class, IF(Type='Credit',Amount,0) Credit,  IF(Type='Debit',Amount,0) Debit FROM Utility_Info WHERE Date>=Date1 AND Date<=Date2
  UNION ALL
  SELECT Date, (Transaction_Id) Id, ('Interest Pay') Class, 0 Credit,  (Amount) Debit FROM Transaction WHERE Type='Interest' AND Date>=Date1 AND Date<=Date2) A ORDER BY Date;
END;
|
DELIMITER ;

DROP PROCEDURE IF EXISTS getValue;
DELIMITER |
CREATE PROCEDURE getValue(FromDate DATE)
BEGIN 
  SELECT (IF(ISNULL(A.Memo),0,A.Memo)+IF(ISNULL(A.Forward_Memo),0,A.Forward_Memo)+IF(ISNULL(A. Delivery),0,A. Delivery)+IF(ISNULL(A.Good),0,A.Good)+IF(ISNULL(A.Utility),0,A.Utility)+IF(ISNULL(A.Interest),0,A.Interest)) Value FROM (SELECT (SELECT SUM(Total-Discount) FROM Memo_Info WHERE Date < FromDate) Memo, (SELECT SUM(Total-Discount) FROM Forward_Memo_Info WHERE Date < FromDate) Forward_Memo, (SELECT SUM(Total-Discount) FROM Forward_Memo_Delivery WHERE Date < FromDate) Delivery, (SELECT SUM(Discount-Total) FROM Good_Info WHERE Date < FromDate) Good, (SELECT SUM(IF(Type='Credit',Amount,-Amount)) FROM Utility_Info WHERE Date < FromDate) Utility, (SELECT SUM(-Amount) FROM Transaction WHERE Type='Interest' AND Date < FromDate) Interest) AS A;
END;
|
DELIMITER ;