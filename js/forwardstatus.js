function formatdelivery(connection, Delivery, listDelivery, table, design){
  design += '<div class="col-md-12">';
  design += '<div class="col-md-3"><b>Delivery Id #'+Delivery.Delivery_Id+'</b><br>Date: '+Delivery.Date.toString().slice(0,15)+'<br>Billing Address: '+Delivery.Delivery_Address+'</div>';
  design += '<div class="col-md-6">';
  design += ObjtoTable(Delivery.deliveries);
  design += '</div>';
  design += '<div class="col-md-12" style="margin: 10px"></div>';
  design += '<div class="col-md-3" style="position: absolute; bottom: 15px; right: 15px">';
  design += 'Load Amount: <div style="float:right">'+Delivery.Load_Amount+'</div><br>';
  design += 'Transport: <div style="float:right">'+Delivery.Transport+'</div><br>';
  design += '<div style="border-top: 1px solid grey"></div>';
  design += 'Total: <div style="float:right">'+Delivery.Total+'</div><br>';
  design += 'Paid: <div style="float:right">'+Delivery.Paid+'</div><br>';
  design += 'Discount: <div style="float:right">'+Delivery.Discount+'</div><br>';
  design += '<div style="border-top: 1px solid grey"></div>';
  design += 'Due: <div style="float:right">'+Delivery.Due+'</div><br>';
  design += '</div></div>'; 
  generatedeliveryrows(connection, listDelivery, table, design);
}

function generatedeliveryrows(connection, listDelivery, table, design, ListMemo){
  console.log(listDelivery.length);
  if(listDelivery.length == 0){
    design += '</div>';
    design += '<div class="expand-arrow text-center" style="padding: 10px">Deliveries<br><span class="glyphicon glyphicon-menu-down"></span></div>'
    design += '</div>';
    console.log(design);
    generateforwardrows(connection, ListMemo, table, design);
    return;
  }
  var Delivery = listDelivery.pop();
  connection.query("SELECT `Brand`, `Product`, `Unit`, `Quantity`  FROM `Forward_Memo_Delivery_Good` LEFT JOIN `Good` ON Forward_Memo_Delivery_Good.Good_Id = Good.Good_Id WHERE Delivery_Id = ?",[Delivery.Delivery_Id],function(error, delivery, fields){
        if(error){
          alert("Something is wrong");
        }
        else{
          Delivery.deliveries = delivery;
          formatdelivery(connection, Delivery, listDelivery, table, design);
        } 
    });
}

function getdelivery(connection, Memo_Id, table, design, ListMemo){
  var ret = '';
  connection.query("SELECT `Date`, `Delivery_Id`, `DO_No`, `Delivery_Address`, `Load_Amount`, `Transport`, `Total`, `Paid`, `Discount`, `Due` FROM `Forward_Memo_Delivery` WHERE Memo_Id = ?",[Memo_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
        generatedeliveryrows(connection, result, table, design, ListMemo);
      } 
  });
}

function formatforwardstatement(Memo){
  console.log('format statelemt');
  var ret = '';
  ret += '<div><b>Memo Id #'+Memo.Memo_Id+'</b> (Date: '+Memo.Date.toString().slice(0,15)+')</div>';
  ret += ObjtoTable(Memo.purchases);
  return ret;
}

function generateforwardrows(connection, ListMemo, table, design){
  console.log(JSON.stringify(ListMemo));
  if(ListMemo === undefined || ListMemo.length == 0 ){
    table.append(design);
    connection.release();
    return;
  }
  var Memo = ListMemo.pop();
  console.log(JSON.stringify(ListMemo));
  connection.query("SELECT `Brand`, `Product`, `Unit`, `Quantity`, `Rate`, `Amount` FROM `Good` LEFT JOIN `Forward_Memo_Purchase` ON Good.Good_Id = Forward_Memo_Purchase.Good_Id WHERE Memo_Id = ?",[Memo.Memo_Id],function(error, purchase, fields){
        if(error){
          alert("Something is wrong");
        }
        else{
          Memo.purchases = purchase;
          design += '<div class="col-md-12 list-box">';
          design += '<div class="col-md-12">';
          design += '<div class="col-md-9">';
          design += formatforwardstatement(Memo);
          design += '</div>';
          design += '<div class="col-md-3" style="position: absolute; bottom: 15px; right: 15px">';
          design += 'Total: <div style="float:right">'+Memo.Total+'</div><br>';
          design += 'Paid: <div style="float:right">'+Memo.Paid+'</div><br>';
          design += 'Discount: <div style="float:right">'+Memo.Discount+'</div><br>';
          design += '<div style="border-top: 1px solid grey"></div>';
          design += 'Due: <div style="float:right">'+Memo.Due+'</div><br>';
          design += '</div></div>';
          design += '<div class="col-md-12" style="border-top: 1px solid gainsboro; margin-bottom: 10px"></div>';
          design += '<div class="expandable-div" style="display: none">';
          getdelivery(connection, Memo.Memo_Id,table, design, ListMemo);
        } 
    });
}

function getforwardstatusid(Memo_Id, table){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("SELECT `Date`, `Memo_Id`, `DO_No`, `Party_Name`, `Party_Address`, `Mobile_No`, `Total`, `Paid`, `Discount`, `Due` FROM `Forward_Memo_Info` LEFT JOIN `Party` ON Forward_Memo_Info.Party_Id = Party.Party_Id  WHERE Memo_Id = ? ORDER BY `Date`",[Memo_Id],function(error, result, fields){
      if(error){
        alert("Here Something is wrong");
      }
      else{
         var design = '';
          design += '<div class="col-md-6"><table class="table table-no-border table-condensed"><tbody>';
         design += '<tr><td class="col-md-3">DO No:</td><td class="col-md-9">'+result[0].DO_No+'</td></tr>';
         design += '<tr><td class="col-md-3">Party Name:</td><td class="col-md-9">'+result[0].Party_Name+'</td></tr>';
         design += '<tr><td class="col-md-3">Party Address:</td><td class="col-md-9">'+result[0].Party_Address+'</td></tr>';
         design += '<tr><td class="col-md-3">Mobile No:</td><td class="col-md-9">'+result[0].Mobile_No+'</td></tr>';
         design += '</tbody></table></div>';
         design +=  '<div class="col-md-12" style="margin-bottom: 10px; border-top: 1px solid grey"></div>';
         
         generateforwardrows(connection, result, table, design);
      }
    });
  });
}

