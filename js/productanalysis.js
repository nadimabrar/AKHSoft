function getproductanalysis(Good_Id, Date1, Date2){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("call getProductAnalysis(?, ?, ?)",[Date1, Date2, Good_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        var step=result[0].length;
        step= Math.floor(step/6);
        if(step == 0) step=1;
        for(i=0;i<result[0].length-1;i++){
          if(i%step != 0){
            result[0][i].Date="";
          }
        }
        var data = {
          labels: result[0].map(function(a) {
            if(a.Date == "") { return a.Date; }
            else { return a.Date.toJSON().slice(0,10); } 
          }),
          datasets: [
                  {
                      label: "Product Analysis",
                      fillColor: "rgba(151,187,205,0.2)",
                      strokeColor: "rgba(151,187,205,1)",
                      pointColor: "rgba(151,187,205,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(151,187,205,1)",
                      data: result[0].map(function(a) {return a.Quantity;})
                  }
              ]
        };
        var ctx = document.getElementById("AnalysisGraph").getContext("2d");
        var options = {};
        var Graph = new Chart(ctx).Line(data, options);
        $('#Analysis').html('');
        connection.release();
      }
    });
  });
}