function insertGoods(connection, table, scope){
  var Memo_Id = scope.find('input[name="Memo_Id"]').val();
  var ListGood = [];
  scope.find('.purchase-row').each(function(){
    ListGood.push($(this));
  });
  insertGood(ListGood, connection, Memo_Id, table, scope);
}

function insertGood(ListGood, connection, Memo_Id, table, scope){
  if(ListGood.length == 0){
    scope.find('.purchase-row').parent().html('');
    scope.find('.add-row-btn-good-memo-entry').click();
    scope.find('input').val('');
    var date = new Date().toJSON().slice(0,10);
    scope.find('input[type="date"]').val(date);
    scope.find('input[name="Party_Id"]').val('-1');
    scope.find('input[name="Supplier_Id"]').val('-1');
    scope.find('input[name="Good_Id"]').val('-1');
    connection.commit(function(error) {
      if (error) { 
        return connection.rollback(function() {
          throw error;
        });
      }
      console.log('Transaction Complete.');

      connection.release();
    });
  }                  
  else{
    var Good = ListGood.pop();
    var idfield = Good.find('input[name="Good_Id"]');
    var Good_Id;
    if(idfield.val()=='-1'){
      var newGood = {};
      newGood["Brand"] = Good.find('input[name="Brand"]').val();
      newGood["Product"] = Good.find('input[name="Product"]').val();
      newGood["Unit"] = Good.find('input[name="Unit"]').val();
      connection.query("INSERT INTO ?? SET ?",['Good', newGood],function(error, result, fields){
        if(error){
          return connection.rollback(function(){
            throw error;
          });
        }
        else{
          Good_Id = result.insertId;
          var goodObj = {};
  
          goodObj["Memo_Id"] = Memo_Id;
          goodObj["Good_Id"] = Good_Id;
          goodObj["Rate"] = Good.find('input[name="Rate"]').val();
          goodObj["Quantity"] = Good.find('input[name="Quantity"]').val();
          goodObj["Amount"] = Good.find('input[name="Amount"]').val();
          
          connection.query('INSERT INTO ?? SET ?', [table, goodObj], function(error, result, fields){
            if(error){
              return connection.rollback(function(){
                throw error;
              });
            }
            else{
              insertGood(ListGood, connection, Memo_Id, table, scope)
            }
          });
        }
      });
    }
    else{
      Good_Id = idfield.val();
      var goodObj = {};
  
      goodObj["Memo_Id"] = Memo_Id;
      goodObj["Good_Id"] = Good_Id;
      goodObj["Rate"] = Good.find('input[name="Rate"]').val();
      goodObj["Quantity"] = Good.find('input[name="Quantity"]').val();
      goodObj["Amount"] = Good.find('input[name="Amount"]').val();

      connection.query('INSERT INTO ?? SET ?', [table, goodObj], function(error, result, fields){
        if(error){
          return connection.rollback(function(){
            throw error;
          });
        }
        else{
          insertGood(ListGood, connection, Memo_Id, table, scope)
        }
      });
    }
  }
}
  
  
function insertMemoInfo(connection, Party_Id, scope){
  var infoObj = {}
  
  infoObj["Date"] = scope.find('input[name="Date"]').val();
  infoObj["Memo_Id"] = scope.find('input[name="Memo_Id"]').val();
  infoObj["DO_No"] = scope.find('input[name="DO_No"]').val();
  infoObj["Party_Id"] = Party_Id;
  infoObj["Billing_Address"] = scope.find('input[name="Billing_Address"]').val();
  infoObj["Sub_Total"] = scope.find('input[name="Sub_Total"]').val();
  infoObj["Load_Amount"] = scope.find('input[name="Load_Amount"]').val();
  infoObj["Transport"] = scope.find('input[name="Transport"]').val();
  infoObj["Total"] = scope.find('input[name="Total"]').val();
  infoObj["Paid"] = scope.find('input[name="Paid"]').val();
  infoObj["Discount"] = scope.find('input[name="Discount"]').val();
  infoObj["Due"] = scope.find('input[name="Due"]').val();
  
  connection.query('INSERT INTO ?? SET ?', ['Memo_Info', infoObj], function(error, result, fields){
    if(error){
      return connection.rollback(function(){
        throw error;
      });
    }
    insertGoods(connection, 'Memo_Purchase', scope);
  });
}


function insertMemo(connection, scope){
  var idfield = scope.find('input[name="Party_Id"]');
  var Party_Id;
  if(idfield.val() == '-1'){
    var newParty = {};
    newParty["Party_Name"] = scope.find('input[name="Party_Name"]').val();
    newParty["Party_Address"] = scope.find('input[name="Party_Address"]').val();
    newParty["Mobile_No"] = scope.find('input[name="Mobile_No"]').val();
    connection.query("INSERT INTO ?? SET ?",['Party', newParty],function(error, result, fields){
      if(error){
        return connection.rollback(function(){
          throw error;
        });
      }
      else{
        Party_Id = result.insertId;
        insertMemoInfo(connection, Party_Id, scope); 
      }
    });
  }
  else{
    Party_Id = idfield.val();
    insertMemoInfo(connection, Party_Id, scope);
  }
}
  

function insertMemoTransaction(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      connection.query("SELECT ( SELECT 1 FROM `Memo_Info` WHERE `Memo_Id` = ? ) AS `Flag`",[scope.find('input[name="Memo_Id"]').val()],function(error, result, fields){
        if(result[0].Flag == 1){
          alert("Memo of same Id already exists!");
        }
        else{
           insertMemo(connection, scope);
        }
      }); 
    });
  });
}


function insertGoodPurInfo(connection, Supplier_Id, scope){
  var infoObj = {}
  
  infoObj["Date"] = scope.find('input[name="Date"]').val();
  infoObj["Memo_Id"] = scope.find('input[name="Memo_Id"]').val();
  infoObj["DO_No"] = scope.find('input[name="DO_No"]').val();
  infoObj["Supplier_Id"] = Supplier_Id;
  infoObj["Sub_Total"] = scope.find('input[name="Sub_Total"]').val();
  infoObj["Load_Amount"] = scope.find('input[name="Load_Amount"]').val();
  infoObj["Transport"] = scope.find('input[name="Transport"]').val();
  infoObj["Total"] = scope.find('input[name="Total"]').val();
  infoObj["Paid"] = scope.find('input[name="Paid"]').val();
  infoObj["Discount"] = scope.find('input[name="Discount"]').val();
  infoObj["Due"] = scope.find('input[name="Due"]').val();
  
  connection.query('INSERT INTO ?? SET ?', ['Good_Info', infoObj], function(error, result, fields){
    if(error){
      return connection.rollback(function(){
        throw error;
      });
    }
    insertGoods(connection, 'Good_Purchase', scope);
  });
}


function insertGoodPur(connection, scope){
  var idfield = scope.find('input[name="Supplier_Id"]');
  var Supplier_Id;
  if(idfield.val() == '-1'){
    var newSupplier = {};
    newSupplier["Supplier_Name"] = scope.find('input[name="Supplier_Name"]').val();
    newSupplier["Supplier_Address"] = scope.find('input[name="Supplier_Address"]').val();
    newSupplier["Mobile_No"] = scope.find('input[name="Mobile_No"]').val();
    connection.query("INSERT INTO ?? SET ?",['Supplier', newSupplier],function(error, result, fields){
      if(error){
        return connection.rollback(function(){
          throw error;
        });
      }
      else{
        Supplier_Id = result.insertId;
        insertGoodPurInfo(connection, Supplier_Id, scope); 
      }
    });
  }
  else{
    Supplier_Id = idfield.val();
    insertGoodPurInfo(connection, Supplier_Id, scope);
  }
}


function insertGoodTransaction(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      connection.query("SELECT ( SELECT 1 FROM `Good_Info` WHERE `Memo_Id` = ? ) AS `Flag`",[scope.find('input[name="Memo_Id"]').val()],function(error, result, fields){
        if(result[0].Flag == 1){
          alert("Memo of same Memo Id already exists!");
        }
        else{
           insertGoodPur(connection, scope);
        }
      });
    });
  });
}


  
function insertForwardMemoInfo(connection, Party_Id, scope){
  var infoObj = {}
  
  infoObj["Date"] = scope.find('input[name="Date"]').val();
  infoObj["Memo_Id"] = scope.find('input[name="Memo_Id"]').val();
  infoObj["DO_No"] = scope.find('input[name="DO_No"]').val();
  infoObj["Party_Id"] = Party_Id;
  infoObj["Total"] = scope.find('input[name="Total"]').val();
  infoObj["Paid"] = scope.find('input[name="Paid"]').val();
  infoObj["Discount"] = scope.find('input[name="Discount"]').val();
  infoObj["Due"] = scope.find('input[name="Due"]').val();
  
  connection.query('INSERT INTO ?? SET ?', ['Forward_Memo_Info', infoObj], function(error, result, fields){
    if(error){
      return connection.rollback(function(){
        throw error;
      });
    }
    insertGoods(connection, 'Forward_Memo_Purchase', scope);
  });
}

function insertForwardMemoPur(connection, scope){
  var idfield = scope.find('input[name="Party_Id"]');
  var Party_Id;
  if(idfield.val() == '-1'){
    var newParty = {};
    newParty["Party_Name"] = scope.find('input[name="Party_Name"]').val();
    newParty["Party_Address"] = scope.find('input[name="Party_Address"]').val();
    newParty["Mobile_No"] = scope.find('input[name="Mobile_No"]').val();
    connection.query("INSERT INTO ?? SET ?",['Party', newParty],function(error, result, fields){
      if(error){
        return connection.rollback(function(){
          throw error;
        });
      }
      else{
        Party_Id = result.insertId;
        insertForwardMemoInfo(connection, Party_Id, scope); 
      }
    });
  }
  else{
    Party_Id = idfield.val();
    insertForwardMemoInfo(connection, Party_Id, scope);
  }
}

function insertForwardMemoTransaction(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      connection.query("SELECT ( SELECT 1 FROM `Forward_Memo_Info` WHERE `Memo_Id` = ? ) AS `Flag`",[scope.find('input[name="Memo_Id"]').val()],function(error, result, fields){
        if(result[0].Flag == 1){
          alert("Memo of same Forward Memo Id already exists!");
        }
        else{
           insertForwardMemoPur(connection, scope);
        }
      });
    });
  });
}


function insertForwardGoodPurInfo(connection, Supplier_Id, scope){
  var infoObj = {}
  
  infoObj["Date"] = scope.find('input[name="Date"]').val();
  infoObj["Memo_Id"] = scope.find('input[name="Memo_Id"]').val();
  infoObj["DO_No"] = scope.find('input[name="DO_No"]').val();
  infoObj["Supplier_Id"] = Supplier_Id;
  infoObj["Total"] = scope.find('input[name="Total"]').val();
  infoObj["Paid"] = scope.find('input[name="Paid"]').val();
  infoObj["Discount"] = scope.find('input[name="Discount"]').val();
  infoObj["Due"] = scope.find('input[name="Due"]').val();
  connection.query('INSERT INTO ?? SET ?', ['Forward_Good_Info', infoObj], function(error, result, fields){
    if(error){
      return connection.rollback(function(){
        throw error;
      });
    }
    insertGoods(connection, 'Forward_Good_Purchase', scope);
  });
}


function insertForwardGoodPur(connection, scope){
  var idfield = scope.find('input[name="Supplier_Id"]');
  var Supplier_Id;
  if(idfield.val() == '-1'){
    var newSupplier = {};
    newSupplier["Supplier_Name"] = scope.find('input[name="Supplier_Name"]').val();
    newSupplier["Supplier_Address"] = scope.find('input[name="Supplier_Address"]').val();
    newSupplier["Mobile_No"] = scope.find('input[name="Mobile_No"]').val();
    connection.query("INSERT INTO ?? SET ?",['Supplier', newSupplier],function(error, result, fields){
      if(error){
        return connection.rollback(function(){
          throw error;
        });
      }
      else{
        Supplier_Id = result.insertId;
        insertForwardGoodPurInfo(connection, Supplier_Id, scope); 
      }
    });
  }
  else{
    Supplier_Id = idfield.val();
    insertForwardGoodPurInfo(connection, Supplier_Id, scope);
  }
}


function insertForwardGoodTransaction(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      connection.query("SELECT ( SELECT 1 FROM `Forward_Good_Info` WHERE `Memo_Id` = ? ) AS `Flag`",[scope.find('input[name="Memo_Id"]').val()],function(error, result, fields){
        if(result[0].Flag == 1){
          alert("Memo of same Memo Id already exists!");
        }
        else{
           insertForwardGoodPur(connection, scope);
        }
      });
    });
  });
}

// Delivery


function insertDeliveryGood(ListGood, connection, Delivery_Id, table, scope){
  if(ListGood.length == 0){
    scope.find('.purchase-row').parent().html('');
    scope.find('.add-row-delivery-btn').click();
    scope.find('input').val('');
    var date = new Date().toJSON().slice(0,10);
    scope.find('input[type="date"]').val(date);
    connection.commit(function(error) {
      if (error) { 
        return connection.rollback(function() {
          throw error;
        });
      }
      console.log('Transaction Complete.');

      connection.release();
    });
  }                  
  else{
    var Good = ListGood.pop();
    var Good_Id = Good.find('input[name="Good_Id"]').val();
    var goodObj = {};

    goodObj["Delivery_Id"] = Delivery_Id;
    goodObj["Good_Id"] = Good_Id;
    goodObj["Quantity"] = Good.find('input[name="Quantity"]').val();

    connection.query('INSERT INTO ?? SET ?', [table, goodObj], function(error, result, fields){
      if(error){
        return connection.rollback(function(){
          throw error;
        });
      }
      else{
        insertDeliveryGood(ListGood, connection, Delivery_Id, table, scope);
      }
    });
  }
}

function insertDeliveryGoods(connection, Delivery_Id, table, scope){
  var ListGood = [];
  scope.find('.purchase-row').each(function(){
    ListGood.push($(this));
  });
  insertDeliveryGood(ListGood, connection, Delivery_Id, table, scope);
}

function insertDeliveryMemo(connection, scope){
   var infoObj = {}
  
  infoObj["Date"] = scope.find('input[name="Date"]').val();
  infoObj["Memo_Id"] = scope.find('input[name="Memo_Id"]').val();
  infoObj["Delivery_Id"] = scope.find('input[name="Delivery_Id"]').val();
  infoObj["DO_No"] = scope.find('input[name="DO_No"]').val();
  infoObj["Delivery_Address"] = scope.find('input[name="Delivery_Address"]').val();
  infoObj["Load_Amount"] = scope.find('input[name="Load_Amount"]').val();
  infoObj["Transport"] = scope.find('input[name="Transport"]').val();
  infoObj["Total"] = scope.find('input[name="Total"]').val();
  infoObj["Paid"] = scope.find('input[name="Paid"]').val();
  infoObj["Discount"] = scope.find('input[name="Discount"]').val();
  infoObj["Due"] = scope.find('input[name="Due"]').val();
  console.log(JSON.stringify(infoObj));
  var qry = connection.query('INSERT INTO ?? SET ?', ['Forward_Memo_Delivery', infoObj], function(error, result, fields){
    if(error){
      console.log(error);
      return connection.rollback(function(){
        throw error;
      });
    }
    else{
      var Delivery_Id = result.insertId;
      insertDeliveryGoods (connection, Delivery_Id, 'Forward_Memo_Delivery_Good', scope);
    }
  });
}


function insertDeliveryMemoTransaction(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      connection.query("SELECT ( SELECT 1 FROM `Forward_Memo_Info` WHERE `Memo_Id` = ? ) AS `Flag`",[scope.find('input[name="Memo_Id"]').val()],function(error, result, fields){
        if(result[0].Flag == 1){
          insertDeliveryMemo(connection, scope);
        }
        else{
           alert("No Forward Memo Id doesn't exist!");
        }
      });
    });
  });
}


function insertUtility(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      var infoObj = {}
      infoObj["Date"] = scope.find('input[name="Date"]').val();
      infoObj["Class"] = scope.find('input[name="Class"]').val();
      infoObj["Note"] = scope.find('input[name="Note"]').val();
      infoObj["Type"] = scope.find('select').val();
      infoObj["Amount"] = scope.find('input[name="Amount"]').val();
      var sql = mysql.format("INSERT INTO `Utility_Info` SET ?",[infoObj]);
      console.log(sql);
      connection.query("INSERT INTO `Utility_Info` SET ?",[infoObj],function(error, result, fields){
        if(error){
          console.log(error);
          return connection.rollback(function(){
            throw error;
          });
        }
        else{
          connection.commit(function(error) {
            if (error) { 
              return connection.rollback(function() {
                throw error;
              });
            }
            console.log('Transaction Complete.');

            connection.release();
          });
          scope.find('input[type!="date"]').val('');
        }
      });
    });
  });
}


function insertDue(scope){
  pool.getConnection(function(error, connection){
    if(error){
      alert("Somthing is wrong with your connection");
    }
    connection.beginTransaction(function(error){
      if(error) { throw error; }
      var infoObj = {}
      infoObj["Date"] = scope.find('input[name="Date"]').val();
      infoObj["Party_Id"] = scope.find('input[name="Party_Id"]').val();
      infoObj["Value"] = scope.find('input[name="Amount"]').val();
      var sql = mysql.format("INSERT INTO `Utility_Info` SET ?",[infoObj]);
      console.log(sql);
      connection.query("INSERT INTO `Due_Payment` SET ?",[infoObj],function(error, result, fields){
        if(error){
          console.log(error);
          return connection.rollback(function(){
            throw error;
          });
        }
        else{
          connection.commit(function(error) {
            if (error) { 
              return connection.rollback(function() {
                throw error;
              });
            }
            console.log('Transaction Complete.');

            connection.release();
          });
          scope.find('input[type!="date"]').val('');
        }
      });
    });
  });
}