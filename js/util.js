function ObjtoTable(result){
  var display = '<div style="margin-top: 15px"></div>';
  if (result.length === 0) {
      display += '<h5 class="text-center"> No data found! </h5>';
  } 
  else {
      display += '<table class="table table-bordered"><thead>'; // table-condensed
      display += '<tr>';
      var key;
      for (key in result[0]) {
          display += '<th>' + key + '</th>';
      }
      display += '</tr>';
      display += '</thead>';
      display += '<tbody>';
      for (var i = 0; i < result.length; i++) {
          display += '<tr>';
          for (key in result[i]) {
              if(key=="Date") display += '<td>' + (result[i][key]).toString().slice(0,15) + '</td>';
              else display += '<td>' + result[i][key] + '</td>';
          }
          display += '</tr>';
      }
      display += '</tbody></table>';
  }
  return display;
}