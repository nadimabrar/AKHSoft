 function getmemostatusid(Memo_Id, table){
   pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("SELECT `Date`, `Memo_Id`, `DO_No`, `Party_Name`, `Party_Address`, `Mobile_No`, `Billing_Address`,`Sub_Total`, `Load_Amount`, `Transport`, `Total`, `Paid`, `Discount`, `Due` FROM `Memo_Info` LEFT JOIN `Party` ON Memo_Info.Party_Id = Party.Party_Id  WHERE Memo_Id = ?",[Memo_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
        var design = '';
        design += '<div class="col-md-6"><table class="table table-no-border table-condensed"><tbody>';
        design += '<tr><td class="col-md-3">DO No:</td><td class="col-md-9">'+result[0].DO_No+'</td></tr>';
         design += '<tr><td class="col-md-3">Party Name:</td><td class="col-md-9">'+result[0].Party_Name+'</td></tr>';
         design += '<tr><td class="col-md-3">Party Address:</td><td class="col-md-9">'+result[0].Party_Address+'</td></tr>';
         design += '<tr><td class="col-md-3">Mobile No:</td><td class="col-md-9">'+result[0].Mobile_No+'</td></tr>';
         design += '</tbody></table></div>';
         design +=  '<div class="col-md-12" style="margin-bottom: 10px; border-top: 1px solid grey"></div>';
        table.html(design);
        var Memo = result[0];
        console.log(JSON.stringify(Memo));
         connection.query("SELECT `Brand`, `Product`, `Unit`, `Quantity`, `Rate`, `Amount` FROM `Good` LEFT JOIN `Memo_Purchase` ON Good.Good_ID = Memo_Purchase.Good_ID WHERE Memo_Id = ?",[Memo_Id],function(error, purchase, fields){
            if(error){
              alert("Something is wrong");
            }
            else{
              var design = '';
              Memo.purchases = purchase;
              design += '<div class="col-md-12 list-box">';
              design += '<div class="col-md-9">';
              design += '<div><b>Memo Id #'+Memo.Memo_Id+'</b> (Date: '+Memo.Date.toString().slice(0,15)+')</div>';
              design += ObjtoTable(Memo.purchases);
              design += '<div>Billing Address: '+Memo.Billing_Address+'</div>';
              design += '</div>';
              design += '<div class="col-md-3" style="position: absolute; bottom: 15px; right: 15px">';
              design += 'Sub Total: <div style="float:right">'+Memo.Sub_Total+'</div><br>';
              design += 'Load Amount: <div style="float:right">'+Memo.Load_Amount+'</div><br>';
              design += 'Transport: <div style="float:right">'+Memo.Transport+'</div><br>';
              design += '<div style="border-top: 1px solid grey"></div>';
              design += 'Total: <div style="float:right">'+Memo.Total+'</div><br>';
              design += 'Paid: <div style="float:right">'+Memo.Paid+'</div><br>';
              design += 'Discount: <div style="float:right">'+Memo.Discount+'</div><br>';
              design += '<div style="border-top: 1px solid grey"></div>';
              design += 'Due: <div style="float:right">'+Memo.Due+'</div><br>';
              design +=  '</div>';
              design +=  '</div>';
              table.append(design);
            } 
        });
      }
    });
  });
 }



