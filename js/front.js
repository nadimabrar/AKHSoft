$(document).on('click','.print-btn',function() {
    const {BrowserWindow} = require('electron').remote;
    let win = new BrowserWindow({width: 1200, height: 750});
    win.loadURL(`file://${__dirname}/print.html`);
    win.webContents.print({silent: false, printBackground: false});
});

function syncloop(){
  syncautocompletedata();
  setTimeout(function(){
    syncloop();
  },30*60*1000);
}


// Auto Complete

$(document).on('keyup','input',function(){
    latest = $(this);               
});

// Date


// Window

$(document).on('click', '.close-btn', function() {
    var remote = require('electron').remote;
    var window = remote.getCurrentWindow();
    window.close();
});

$(document).on('click', '.minim-btn', function() {
    var remote = require('electron').remote;
    var window = remote.getCurrentWindow();
    window.minimize();
});


// Auto Calculation
$(document).on('keyup', '.updgroup-good-memo-entry', function() {
    var scope = $(this).parents('.inputroot').first();
    var subtotal = 0;
    scope.find('tbody tr').each(function(){
      var rate = $(this).find('.rate').first().val();
      var quantity = $(this).find('.quantity').first().val();
      if (rate !== '' && quantity !== '' && !isNaN(rate) && !isNaN(quantity)) {
        var total = (parseFloat(rate) * parseFloat(quantity)).toFixed(2);
        subtotal = parseFloat(subtotal) + parseFloat(total);
        $(this).find('.total').first().val(total);
      }
      else{
        $(this).find('.total').first().val('');
      }
    });
    subtotal = parseFloat(subtotal).toFixed(2); 
    var grandtotal;
    if(parseFloat(subtotal)>0){             
       scope.find('.subtotal').first().val(subtotal);
    }
    else{
       scope.find('.subtotal').first().val('');
    }
    subtotal = scope.find('.subtotal').first().val();
    if(subtotal!==''){
      var loadamount = scope.find('.loadamount').first().val();
      if(loadamount!='' && !isNaN(loadamount)){
        grandtotal = parseFloat(subtotal) + parseFloat(loadamount);
        var transport = scope.find('.transport').first().val();
        if(transport!='' && !isNaN(transport)){
          grandtotal = parseFloat(grandtotal) + parseFloat(transport);
          grandtotal = grandtotal.toFixed(2);
          scope.find('.grandtotal').first().val(grandtotal);
        }
        else{
          scope.find('.grandtotal').first().val('');
        }
      }
      else{
        scope.find('.grandtotal').first().val('');
      }
    }
    else{
      scope.find('.grandtotal').first().val('');
    }
    
    grandtotal = scope.find('.grandtotal').first().val();
    var due;
    if(grandtotal!==''){
      var payment = scope.find('.payment').first().val();
      if(payment!='' && !isNaN(payment)){
        due = parseFloat(grandtotal) - parseFloat(payment);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
      else{
        scope.find('.due').first().val('');
      }
    }
    if(due!=='' && !isNaN(due)){ 
      var discount = scope.find('.discount').first().val();
      if(discount!='' && !isNaN(discount)){
        due = parseFloat(due) - parseFloat(discount);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
    }
    else{
      scope.find('.due').first().val('');
    }
});

$(document).on('keyup', '.updgroup-forward-good-memo-entry', function() {
    var scope = $(this).parents('.inputroot').first();
    var grandtotal = 0;
    scope.find('tbody tr').each(function(){
      var rate = $(this).find('.rate').first().val();
      var quantity = $(this).find('.quantity').first().val();
      if (rate !== '' && quantity !== '' && !isNaN(rate) && !isNaN(quantity)) {
        var total = (parseFloat(rate) * parseFloat(quantity)).toFixed(2);
        grandtotal = parseFloat(grandtotal) + parseFloat(total);
        $(this).find('.total').first().val(total);
      }
      else{
        $(this).find('.total').first().val('');
      }
    });
    grandtotal = parseFloat(grandtotal).toFixed(2); 
    if(parseFloat(grandtotal)>0){             
       scope.find('.grandtotal').first().val(grandtotal);
    }
    else{
       scope.find('.grandtotal').first().val('');
    }
  
    grandtotal = scope.find('.grandtotal').first().val();
    var due;
    if(grandtotal!==''){
      var payment = scope.find('.payment').first().val();
      if(payment!='' && !isNaN(payment)){
        due = parseFloat(grandtotal) - parseFloat(payment);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
      else{
        scope.find('.due').first().val('');
      }
    }
    if(due!=='' && !isNaN(due)){ 
      var discount = scope.find('.discount').first().val();
      if(discount!='' && !isNaN(discount)){
        due = parseFloat(due) - parseFloat(discount);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
    }
    else{
      scope.find('.due').first().val('');
    }
});

$(document).on('keyup', '.updgroup-delivery', function() {
    var scope = $(this).parents('.inputroot').first();
    var subtotal = 0;
    var loadamount = scope.find('.loadamount').first().val();
    if(loadamount!='' && !isNaN(loadamount)){
      grandtotal = parseFloat(subtotal) + parseFloat(loadamount);
      var transport = scope.find('.transport').first().val();
      if(transport!='' && !isNaN(transport)){
        grandtotal = parseFloat(grandtotal) + parseFloat(transport);
        grandtotal = grandtotal.toFixed(2);
        scope.find('.grandtotal').first().val(grandtotal);
      }
      else{
        scope.find('.grandtotal').first().val('');
      }
    }
    else{
      scope.find('.grandtotal').first().val('');
    }

    grandtotal = scope.find('.grandtotal').first().val();
    var due;
    if(grandtotal!==''){
      var payment = scope.find('.payment').first().val();
      if(payment!='' && !isNaN(payment)){
        due = parseFloat(grandtotal) - parseFloat(payment);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
      else{
        scope.find('.due').first().val('');
      }
    }
    if(due!=='' && !isNaN(due)){ 
      var discount = scope.find('.discount').first().val();
      if(discount!='' && !isNaN(discount)){
        due = parseFloat(due) - parseFloat(discount);
        due = due.toFixed(2);
        scope.find('.due').first().val(due);
      }
    }
    else{
      scope.find('.due').first().val('');
    }
});


// Others

function cehckValidity(scope){
  var valid = true;
  scope.find('input').each(function(){
    if($(this).val()===''){
      valid = false;
      return false;
    }
  });
  return valid;
}


$(document).on('click', '.acc-btn', function() {
    var qryObj = {},
        ok = true,
        par = $(this).parents().eq(1);
    par.find('input').each(function() {
        var key = $(this).attr('name');
        var val = $(this).val();
        if (val == "") {
            alert("Branch or password can't be empty!");
            ok = false;
        }
        qryObj[key] = val;
    });
    if (!ok) return;
    var user = qryObj['Branch'];
    var pwd = qryObj['Password'];
    doConnect(user, pwd);
});

$(document).on('click', '.stk-btn', function() {
    var qryObj = {},
        ok = true,
        par = $(this).parent().parent(),
        table = 'table' + par.attr('id');
    par.find('input').each(function() {
        var key = $(this).attr('name');
        var val = $(this).val();
        if (val === '') {
            alert('Check Your Inputs Carefully!');
            ok = false;
            return false;
        }
        qryObj[key] = val;
    });
    if (!ok) return false;
    stockq(qryObj);
    par.find('input').each(function() {
        $(this).val('');
    });
});


$(document).on('click', '.add-btn-memo-entry', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertMemoTransaction(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});

$(document).on('click', '.add-btn-good-entry', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertGoodTransaction(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});

$(document).on('click', '.add-btn-forward-memo-entry', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertForwardMemoTransaction(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});


$(document).on('click', '.add-btn-forward-good-entry', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertForwardGoodTransaction(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});

$(document).on('click', '.add-btn-delivery-memo', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertDeliveryMemoTransaction(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});

$(document).on('click', '.add-btn-utility', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertUtility(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});




$(document).on('click', '.add-btn-due', function() {
    var scope = $(this).parent();
    if(cehckValidity(scope)){
      insertDue(scope);
    }
    else{
      alert('Check your inputs carefully');
    }
});


$(document).on('click', '.add-row-btn-good-memo-entry', function() {
    var tablebody = $(this).parents().eq(1).find('tbody').first();
    tablebody.append($('#purchase-row-template-good-memo-entry').html());
    console.log(tablebody.html());
    initAutoComplete();
});


$(document).on('click', '.add-row-delivery-btn', function() {
    var tablebody = $(this).parents().eq(1).find('tbody').first();
    tablebody.append($('#purchase-row-template-delivery').html());
    console.log(tablebody.html());
    initAutoComplete();
});



$(document).on('dblclick', '.purchase-row', function() {
    var tablebody = $(this).parent().find('tr');
    if(tablebody.length>1){
      $(this).remove();
      $('.updgroup-good-memo-entry').keyup();
      $('.updgroup-forward-good-memo-entry').keyup();
      $('.updgroup-delivery').keyup();
    }
});

$(document).on('click', '.bank-card', function() {
  $('#bankop').find('#History').html('');
  $('#bankop').find('#Transactions').html('');
  $('#bankop').find('#BalanceSheet').html('');
  $('#bankop').find('input[name="Bank_Id"]').val($(this).find('input[name="Ext_Bank_Id"]').val());
  $('#bankop').find('.modal-title').html('Balance Sheet Operations<br>'+$(this).find('input[name="Ext_Bank_Tag"]').val());
  $('#bankop').modal();
});



$(document).on('click', '.fetch-btn', function() {
    var ok = true,
        par = $(this).parent(),
        table = par.find('select').first().val();
    var date = [];
    par.find('input').each(function() {
        date.push($(this).val());
        if ($(this).val() == '') {
            ok = false;
            alert('Dates can\'t be empty');
            return false;
        }
    });
    if (!ok) return;
    fetchdb(table, date);
});

$(document).on('click', '.add-bnk-btn', function() {
    var qryObj = {},
        ok = true,
        par = $(this).parent().parent();
    par.find('input').each(function() {
        var key = $(this).attr('name');
        var val = $(this).val();
        qryObj[key] = val;
        if (val == '') {
            alert('Check Your Inputs Carefully!');
            ok = false;
            return false;
        }
        if(key=="Interest" && isNaN(val)){
            alert('Check Your Inputs Carefully!');
            ok = false;
            return false;
        } 
        if(key=="Limit" && isNaN(val)){
            alert('Check Your Inputs Carefully!');
            ok = false;
            return false;
        } 
    });
    if (!ok) return false;
    createbankaccount(qryObj);
});


$(document).on('dblclick', '.clickable-row', function() {
    var id = $(this).attr('id'),
        table = $(this).parent().parent().attr('id');
    $('#rowid').val(id);
    $('#tablename').val(table);
    $('#edit').html($('#' + table.substr(5)).html());
    $('#edit').find('h3').first().hide();
    $('#edit').find('button').first().removeClass('add-btn').addClass('upd-btn');
    $(this).find('td').not(":first").each(function() {
        $('#edit input[name="' + $(this).attr('id') + '"]').val($(this).html());
    });
    $('#edit .rate').keyup();
    $('#rowop').modal();
});

$(document).on('click', '.dlt-row-btn', function() {
    var id = $('#rowid').val(),
        table = $('#tablename').val();
    deleterow(table, id);
    $('#rowop').modal('toggle');
});

$(document).on('click', '.upd-btn', function() {
    var qryObj = {},
        ok = true,
        id = $('#rowid').val();
    table = $('#tablename').val();
    $('#edit').find('input').each(function() {
        var key = $(this).attr('name');
        var val = $(this).val();
        if (val === '' || ((key == 'Rate' || key == 'Quantity' || key == 'Paid' || key == 'Bill' || key == 'Rent') && isNaN(val))) {
            alert('Check Your Inputs Carefully!');
            ok = false;
            return false;
        }
        qryObj[key] = val;
    });
    if (!ok) return false;
    if (table == 'tableMemo' || table == 'tableGood') {
        qryObj['Total'] = (qryObj['Rate'] * qryObj['Quantity']).toFixed(2);
    }
    if (table == 'tableMemo') {
        qryObj['Due'] = (qryObj['Total'] - qryObj['Paid']).toFixed(2);
    }
    updaterow(table, id, qryObj);
    $('#rowop').modal('toggle');
});

$(document).on('click', '.btn-bank-transaction', function() {
    var qryObj = {};
  qryObj["Date"]=$(this).parents().eq(2).find('input[name="Date"]').val();  qryObj["Bank_Id"]=$(this).parents().eq(2).find('input[name="Bank_Id"]').val();
     qryObj["Type"]=$(this).parents().eq(2).find('select[name="Type"]').val();
  qryObj["Amount"]=$(this).parents().eq(2).find('input[name="Amount"]').val();
    qryObj["Note"]=$(this).parents().eq(2).find('input[name="Note"]').val();
    if(qryObj["Note"]=='' || qryObj["Amount"]=='' || isNaN(qryObj["Amount"])){
      alert('Check Your Inputs Carefully!');
      return false;
    }
   console.log(qryObj);
   inserttransaction(qryObj);
});

$(document).on('click', '.limit-interest-edit',function() {
    var par = $(this).parent();
    var Date = par.find('input[name="Date"]').val();
    var Bank_Id = par.parents().eq(2).find('input[name="Bank_Id"]').val();
    var Table = par.find('select[name="Table"]').val();
    var Value = par.find('input[name="Value"]').val();
    updatelimitinterest(Bank_Id, Date, Table, Value);
});


$(document).on('click', '.btn-edit-history',function() {
    var Bank_Id = $(this).parents().eq(3).find('input[name="Bank_Id"]').val();
    getedithistory(Bank_Id);
});

$(document).on('click', '.bnk-fetch-btn', function() {
    var ok = true,
        par = $(this).parent(),
        id = par.parent().attr('id');
    var date = [];
    par.find('input').each(function() {
        date.push($(this).val());
        if ($(this).val() == '') {
            ok = false;
            alert('Dates can\'t be empty');
            return false;
        }
    });
    if (!ok) return;
    fetchtran(id, date);
});

$(document).on('click', '.btn-party-statement', function() {
  var partyObj = {};
  $(this).parents().eq(1).find('input').each(function(){
    partyObj[$(this).attr('name')]=$(this).val();
  });
  $('#party-statement').html('');
  getpartystatement(partyObj, $('#party-statement'));
});

$(document).on('click', '.btn-forward-status', function() {
  var Memo_Id = $(this).parents().eq(1).find('input[name="Memo_Id"]').val();
  $('#forward-status').html('');
  getforwardstatusid(Memo_Id, $('#forward-status'), 1);
});

$(document).on('click', '.btn-memo-status', function() {
  var Memo_Id = $(this).parents().eq(1).find('input[name="Memo_Id"]').val();
  getmemostatusid(Memo_Id, $('#memo-status'));
});

$(document).on('click', '.btn-product-analysis', function() {
  var Good_Id = $(this).parents().eq(1).find('input[name="Good_Id"]').val();
  var Date1 = $(this).parents().eq(1).find('input[name="From"]').val();
  var Date2 = $(this).parents().eq(1).find('input[name="To"]').val();
  console.log(Date1+' '+Date2);
  getproductanalysis(Good_Id, Date1, Date2);
});

$(document).on('click', '.btn-income-statement', function() {
  var Date1 = $(this).parents().eq(1).find('input[name="From"]').val();
  var Date2 = $(this).parents().eq(1).find('input[name="To"]').val();
  console.log(Date1+' '+Date2);
  getincomestatement(Date1, Date2);
});

$(document).on('click', '.btn-fetch-tr', function() {
  var Bank_Id = $(this).parents().eq(4).find('input[name="Bank_Id"]').val();
  var Date1 = $(this).parents().eq(1).find('input[name="From"]').val();
  var Date2 = $(this).parents().eq(1).find('input[name="To"]').val();
  gettransactions(Bank_Id, Date1, Date2);
});

$(document).on('click', '.btn-sheet', function() {
  var Bank_Id = $(this).parents().eq(4).find('input[name="Bank_Id"]').val();
  var FromDate = $(this).parents().eq(1).find('input[name="From"]').val();
  getbalancesheet(Bank_Id, FromDate);
});

$(document).on('click', '.expand-arrow',function(){
  $(this).parent().find('.expandable-div').slideToggle(700);
  var span = $(this).find('span');
  if(span.hasClass('glyphicon glyphicon-menu-down')){
   span.removeClass('glyphicon glyphicon-menu-down').addClass('glyphicon glyphicon-menu-up'); 
  }
  else{
    span.removeClass('glyphicon glyphicon-menu-up').addClass('glyphicon glyphicon-menu-down');
  }
});