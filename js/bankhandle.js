function makerow(Balance, Interest, pst, list, sheet){
  if(list.length == 0){
    $('#BalanceSheet').append('<div style="margin-top: 20px"></div>'+ObjtoTable(sheet));
    return;
  }
  else{
    var item = list.pop();
    var row = {};
    row.Date = item.Date;
    row.ΔBalance = (item.Balance == null ? 0 : item.Balance);
    Balance += row.ΔBalance;
    row.Balance = Balance;
    if(item.Interest !== null) pst = item.Interest; 
    row.Pst = pst;
    row.ΔInterest = parseFloat(Balance*row.Pst/36000.00,2).toFixed(2)-item.Interest_Pay;
    Interest += row.ΔInterest;
    row.Interest = Interest;
    sheet.push(row);
    makerow(Balance, Interest, pst, list, sheet);
  }
}

function makebalancesheet(openingbalance, openinginterest,interest, sheet){
  console.log(JSON.stringify(sheet));
  sheet.reverse();
  var ret = [];
  makerow(openingbalance, openinginterest, interest, sheet, ret)
}

function formatbank(listBank, display){
  if(listBank.length == 0){
    $('#Accounts').html(display);
    return;
  }
  var Bank=listBank.pop();
  display += '<div class="col-md-3">'
  display += '<div class="bank-card">';
  display += '<input class="hide" name="Ext_Bank_Id" value="'+Bank.Bank_Id+'">';
  display += '<input class="hide" name="Ext_Bank_Tag" value="'+Bank.Bank_Name+' ['+Bank.Account_No+']">';
  display += '<b>'+Bank.Bank_Name+'</b><br>';
  display += Bank.Account_No+'<br>';
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    else{
      connection.query("SELECT Value FROM Interest WHERE Bank_Id = ? AND Date <= CURDATE() ORDER BY Date DESC LIMIT 1",[Bank.Bank_Id],function(error, result, fields){
        if(error){
          alert("Something is wrong");
        }
        else{
          display += '<div style="margin: 5px 0px; border-top: 1px solid gainsboro"></div>';
          display += 'Interest Pst: '+result[0].Value+'%<br>';
          connection.query("SELECT Value FROM `Limit` WHERE Bank_Id = ? AND Date <= CURDATE() ORDER BY Date DESC LIMIT 1",[Bank.Bank_Id],function(error, result, fields){
            if(error){
              alert("Something is wrong");
              console.log(error);
            }
            else{
              display += 'Limit: '+result[0].Value+'<br>';
              var prevday = new Date();
              prevday.setDate(prevday.getDate() - 1);
              connection.query("call getBalanceAndInterest(?, ?)",[prevday.toJSON().slice(0,10), Bank.Bank_Id],function(error, result, fields){
                if(error){
                  alert("Something is wrong");
                  console.log(error);
                }
                else{
                  var Interest = result[0][0].Interest;
                  var Balance = result[0][0].Balance;
                  connection.query("SELECT SUM(CASE Type WHEN 'Credit' THEN Amount ELSE -Amount END) Balance FROM `Transaction` WHERE `Date` = ? AND Bank_Id=?",[new Date().toJSON().slice(0,10),Bank.Bank_Id],function(error, result, fields){
                    if(error){
                      alert("Something is wrong");
                      console.log(error);
                    }
                    else{
                      console.log(JSON.stringify(result));
                      Balance += result[0].Balance;
                      display += 'Balance: '+Balance+'<br>';
                      display += 'Interest: '+Interest+'<br>';
                      display += '</div></div>';
                      formatbank(listBank, display);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
                     

function initbankaccount(){
    pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("SELECT `Bank_Id`, `Bank_Name`, `Account_No` FROM Bank_Accounts",function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
        var display = '';
        formatbank(result, display);
      }
    });
  });
}


function createbankaccount(qryObj){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    console.log(JSON.stringify(qryObj));
    var bankObj = {};
    bankObj["Bank_Name"]=qryObj["Bank_Name"];
    bankObj["Account_No"]=qryObj["Account_No"];
    connection.query("INSERT INTO `Bank_Accounts` SET ?",[bankObj],function(error, result, fields){
      if(error){
        alert("Something is wrong");
      }
      else{
        var Id=result.insertId;
        connection.query("INSERT INTO `Interest` (`Date`, `Bank_Id`, `Value`) VALUES (?, ?, ?)",[new Date('01-02-2010').toJSON().slice(0,10), Id,qryObj.Interest],function(error, result, fields){
          if(error){
            alert("Something is wrong");
          }
          else{
            connection.query("INSERT INTO `Limit` (`Date`, `Bank_Id`, `Value`) VALUES (?, ?, ?)",[new Date('01-02-2010').toJSON().slice(0,10), Id,qryObj.Limit],function(error, result, fields){
              if(error){
                alert("Something is wrong");
              }
              else{
                initbankaccount();
                $('#newaccount').find('input[type!="date"]').each(function(){
                    $(this).val('');
                });
                $('#newaccount').modal('toggle');
              }
            });
          }
        });
      }
    });
  });
}


function updatelimitinterest(Bank_Id, Date, Table, Value){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("INSERT INTO ?? (`Date`,`Bank_Id`,`Value`) VALUES (?, ?, ?)",[Table, Date, Bank_Id, Value],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        connection.release();
        initbankaccount();
        $('#bankop').find('input[type!="date"]').each(function(){
            if($(this).attr('name') != "Bank_Id") $(this).val('');
        });
      }
    });
  });
}

function getedithistory(Bank_Id){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("SELECT Date, (Value) `Interest` FROM `Interest` WHERE Bank_Id=? ORDER BY Date DESC",[Bank_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        $('#History').html('<div><b>Interest</b></div>'+ObjtoTable(result));
        connection.query("SELECT Date, (Value) `Limit` FROM `Limit` WHERE Bank_Id=?  ORDER BY Date DESC",[Bank_Id],function(error, result, fields){
          if(error){
            alert("Something is wrong");
            console.log(error);
          }
          else{
            connection.release();
            $('#History').append('<div><b>Limit</b></div>'+ObjtoTable(result));
          }
        });
      }
    });
  });
}

function inserttransaction(qryObj){
    pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    connection.query("INSERT INTO `Transaction` SET ?",[qryObj],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        connection.release();
        initbankaccount();
        $('#bankop').find('input[type!="date"]').each(function(){
            if($(this).attr('name') != "Bank_Id") $(this).val('');
        });
      }
    });
  });
}

function gettransactions(Bank_Id, Date1, Date2){
  console.log(Bank_Id + Date1 + Date2);
   pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
     else{
        connection.query("SELECT `Date`, `Note`, `Type`, `Amount` FROM `Transaction` WHERE Bank_Id = ? AND `Date` BETWEEN ? AND ? ORDER BY `Date` ASC",[Bank_Id, Date1, Date2],function(error, result, fields){
          if(error){
            alert("Something is wrong");
            console.log(error);
          }
          else{
            console.log(result);
            $('#Transactions').html('<div style="margin-top: 20px"></div>'+ObjtoTable(result));
            connection.release();
          }
        });
     }
  });
}

function getbalancesheet(Bank_Id, FromDate){
  pool.getConnection(function(error,connection){
    if(error){
      alert("Somthing is wrong with the connection");
    }
    var prevday = new Date(FromDate);
    prevday.setDate(prevday.getDate() - 1);
    connection.query("call getBalanceAndInterest(?, ?)",[prevday.toJSON().slice(0,10), Bank_Id],function(error, result, fields){
      if(error){
        alert("Something is wrong");
        console.log(error);
      }
      else{
        var openingbalance, openinginterest;
        openingbalance = result[0][0].Balance;
        openinginterest = result[0][0].Interest;
        $('#BalanceSheet').html('<div style="margin-top: 20px"></div><div class="col-md-6 col-md-offset-6">Opening Balance: <div style="float:right">'+openingbalance+'</div><br>Opening Interest: <div style="float:right">'+openinginterest+'</div></div>');
        connection.query("call getBalanceSheet(?,?,?)",[FromDate, new Date().toJSON().slice(0,10), Bank_Id],function(error, result, fields){
          if(error){
            alert("Something is wrong");
            console.log(error);
          }
          else{
            var sheet = result[0];
            connection.query("SELECT Value FROM Interest WHERE Date < ? AND Bank_Id = ? ORDER BY Date DESC LIMIT 1",[FromDate, Bank_Id],function(error, result, fields){
              makebalancesheet(openingbalance, openinginterest,result[0].Value, sheet)
              connection.release();
            });
          }
        });
      }
    });
  });
} 